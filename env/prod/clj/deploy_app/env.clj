(ns deploy-app.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[deploy-app started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[deploy-app has shut down successfully]=-"))
   :middleware identity})

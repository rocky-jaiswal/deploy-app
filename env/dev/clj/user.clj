(ns user
  (:require [deploy-app.config :refer [env]]
            [clojure.spec.alpha :as s]
            [expound.alpha :as expound]
            [mount.core :as mount]
            [deploy-app.core :refer [start-app]]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(defn start []
  (mount/start-without #'deploy-app.core/repl-server))

(defn stop []
  (mount/stop-except #'deploy-app.core/repl-server))

(defn restart []
  (stop)
  (start))



(ns deploy-app.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [deploy-app.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[deploy-app started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[deploy-app has shut down successfully]=-"))
   :middleware wrap-dev})

(ns deploy-app.utils.core
  (:import [org.apache.commons.exec DefaultExecutor]
           [org.apache.commons.exec CommandLine]))

(def shellscript
  (if (System/getProperty "script_path")
    (str (System/getProperty "script_path") "restart-service.sh")
    (str "test-restart-service.sh")))

(defn build-args [param1 param2]
  (-> (new CommandLine "bash")
    (.addArgument shellscript)
    (.addArgument param1)
    (.addArgument param2)))

(defn exec-cmd [param1 param2]
  (if (= 0 (.execute (new DefaultExecutor) (build-args param1 param2)))
    "success"
    "failure"))

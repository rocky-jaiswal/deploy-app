(ns deploy-app.middleware
  (:require [deploy-app.env :refer [defaults]]
            [clojure.tools.logging :as log]
            [deploy-app.config :refer [env]]
            [ring.middleware.flash :refer [wrap-flash]]
            [immutant.web.middleware :refer [wrap-session]]
            [buddy.auth.backends :as backends]
            [buddy.auth.middleware :refer [wrap-authentication]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]))

(defn get-users []
  (if (System/getProperty "admin_password")
    {:admin (System/getProperty "admin_password")}
    {:admin
      ((read-string (slurp "resources/config.edn")) :secret)}))

(defn my-authfn [request authdata]
  (let [username (:username authdata) password (:password authdata)]
    (when-let [user-password ((get-users) (keyword username))]
      (when (= password user-password)
        (keyword username)))))

(def backend (backends/basic {:realm "deploy-app-api"
                              :authfn my-authfn}))

(defn wrap-auth [handler]
  (-> handler
    (wrap-authentication backend)))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-auth
      wrap-flash
      (wrap-session {:cookie-attrs {:http-only true}})
      (wrap-defaults
        (-> site-defaults
            (assoc-in [:security :anti-forgery] false)
            (dissoc :session)))))

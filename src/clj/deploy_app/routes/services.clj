(ns deploy-app.routes.services
  (:require [ring.util.http-response :refer :all]
            [compojure.api.sweet :refer :all]
            [compojure.api.meta :refer [restructure-param]]
            [schema.core :as s]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth :refer [authenticated?]]
            [deploy-app.utils.core :refer :all]))

(defn access-error [_ _]
  (unauthorized {:error "unauthorized"}))

(defn wrap-restricted [handler rule]
  (restrict handler {:handler  rule
                     :on-error access-error}))

(defmethod restructure-param :auth-rules [_ rule acc]
  (update-in acc [:middleware] conj [wrap-restricted rule]))

(defapi service-routes
  {:swagger {:ui "/swagger-ui"
             :spec "/swagger.json"
             :data {:info {:version "1.0.0"
                           :title "Sample API"
                           :description "Sample Services"}}}}

  (context "/__deployer" []
    :tags ["deployer"]

    (GET "/health" []
      :auth-rules authenticated?
      :summary  "basic health check"
      (ok {:msg (str "it " "works!")}))

    (PUT "/service" []
      :auth-rules authenticated?
      :body-params [service-name :- String, image-path :- String]
      :summary     "Update a service"
      (ok {:result (exec-cmd service-name image-path)}))))

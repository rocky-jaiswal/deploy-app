FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/deploy-app.jar /deploy-app/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/deploy-app/app.jar"]
